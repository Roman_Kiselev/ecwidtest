package ru.download;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Callable;

public class FilePerThreadDownloader implements Callable<Integer>{
    private final Logger logger = Logger.getLogger(this.getClass());
    private DownloadGuard downloadGuard;
    private String fileName;
    private String fileUrl;
    private static final int BUFFER_SIZE = 1024;
    private DownloadResult downloadResult;

    public FilePerThreadDownloader(DownloadGuard downloadGuard, String fileName, String fileUrl,
                                   DownloadResult downloadResult) {
        this.downloadGuard = downloadGuard;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.downloadResult = downloadResult;
    }


    @Override
    public Integer call(){
        try(RandomAccessFile targetFile = new RandomAccessFile(new File(fileName), "rw")) {
            targetFile.seek(0);
            byte[] buf = new byte[BUFFER_SIZE];
            URLConnection urlConnection;
            int size = 0;
            URL url = new URL(fileUrl);
            urlConnection = url.openConnection();
            try(BufferedInputStream bufferedInputStream = new BufferedInputStream(
                    urlConnection.getInputStream())) {
                boolean done = false;
                while (!done) {
                    long sleep = downloadGuard.canI();
                    if (sleep > 0) {
                        Thread.sleep(sleep);
                    }
                    int len = bufferedInputStream.read(buf, 0, BUFFER_SIZE);
                    if (len == -1)
                        done = true;
                    else {
                        targetFile.write(buf, 0, len);
                        size += len;
                    }
                    downloadGuard.setDownloaded(len);
                }
            }
            downloadResult.getResultHashMap().put(fileName, true);
            return size;
        } catch (IOException | InterruptedException  e) {
            downloadResult.getResultHashMap().put(fileName, false);
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}
