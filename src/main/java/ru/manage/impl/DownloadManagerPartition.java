package ru.manage.impl;

import org.apache.log4j.Logger;
import ru.download.DownloadGuard;
import ru.download.DownloadResult;
import ru.download.PartitionDownloader;
import ru.entity.DownloadItem;
import ru.manage.interf.DownloadManager;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DownloadManagerPartition implements DownloadManager{
    private final Logger logger = Logger.getLogger(this.getClass());
    private List<DownloadItem> downloadItems;
    private int count;
    private int speed;
    private DownloadResult downloadResult;

    public DownloadManagerPartition(List<DownloadItem> downloadItems, int count, int speed,
                                    DownloadResult downloadResult){
        this.downloadItems = downloadItems;
        this.count = count;
        this.speed = speed;
        this.downloadResult = downloadResult;
    }

    @Override
    public Integer download(){
        try{
            Integer r = 0;
            for (DownloadItem downloadItem: downloadItems) {
                downloadResult.setResult(false);
                URL website = new URL(downloadItem.getUrl());
                HttpURLConnection urlConnection = (HttpURLConnection) website.openConnection();

                urlConnection.connect();
                int currentSize = urlConnection.getContentLength();
                int residue = currentSize % count;
                int begin, end, step;
                if (residue != 0) {
                    step = currentSize / (count - 1);
                    end = currentSize;
                    int resideReCalc = (currentSize % (count - 1) == 0)?residue:currentSize % (count - 1);
                    begin = currentSize - resideReCalc;
                } else {
                    step = currentSize / count;
                    begin = currentSize - step;
                    end = currentSize;
                }
                List<Future<Integer>> result = new ArrayList<>();
                ExecutorService executorService = Executors.newFixedThreadPool(count);
                DownloadGuard guard = new DownloadGuard(speed);
                for (int i = 0; i < count; i++) {
                    result.add(executorService.submit(new PartitionDownloader(end, downloadItem.getFileName(),
                            downloadItem.getUrl(), begin, guard, downloadResult)));
                    end = begin;
                    begin -= step;
                }
                executorService.shutdown();

                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                for (Future<Integer> resultItem : result) {
                    r += resultItem.get();
                }
                downloadResult.getResultHashMap().put(downloadItem.getFileName(), true);
            }
            return r;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}
