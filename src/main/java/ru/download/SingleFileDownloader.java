package ru.download;

import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.Callable;

public class SingleFileDownloader implements Callable<Long> {
    private final Logger logger = Logger.getLogger(this.getClass());
    private String fileName;
    private String url;
    public SingleFileDownloader(String fileName, String url){
        this.fileName = fileName;
        this.url = url;

    }

    @Override
    public Long call() throws Exception {
        try {
            URL website = new URL(url);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            return fos.getChannel().size();
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
}
