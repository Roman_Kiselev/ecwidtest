package ru.manage.impl;

import org.apache.log4j.Logger;
import ru.download.DownloadGuard;
import ru.download.DownloadResult;
import ru.download.FilePerThreadDownloader;
import ru.entity.DownloadItem;
import ru.manage.interf.DownloadManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DownloadManagerSingleFileMultithreading implements DownloadManager {
    private final Logger logger = Logger.getLogger(this.getClass());
    private List<DownloadItem> downloadItems;
    private int count;
    private DownloadGuard guard;
    private DownloadResult downloadResult;

    public DownloadManagerSingleFileMultithreading(List<DownloadItem> downloadItems, int count, DownloadGuard guard,
                                                   DownloadResult downloadResult) {
        this.downloadItems = downloadItems;
        this.count = count;
        this.guard = guard;
        this.downloadResult = downloadResult;
    }


    @Override
    public Integer download() {
        try{
            ExecutorService executorService = Executors.newFixedThreadPool(count);
            List<Future<Integer>> result = new ArrayList<>();
            downloadResult.setResult(false);

            for (DownloadItem downloadItem: downloadItems) {
                result.add(executorService.submit(new FilePerThreadDownloader(guard, downloadItem.getFileName(),
                        downloadItem.getUrl(), downloadResult)));
            }

            executorService.shutdown();
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            Integer r = 0;
            for (Future<Integer> resultItem : result) {
                r += resultItem.get();
            }
            return r;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

}
