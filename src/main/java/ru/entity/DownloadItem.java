package ru.entity;

public class DownloadItem {
    private String url;
    private String fileName;
    public DownloadItem(String url, String fileName){
        this.url = url;
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public String getFileName() {
        return fileName;
    }
}
