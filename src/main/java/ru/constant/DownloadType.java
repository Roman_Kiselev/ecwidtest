package ru.constant;

public enum DownloadType {
    SINGLE,
    PARTITION,
    FILE_PER_THREAD
}
