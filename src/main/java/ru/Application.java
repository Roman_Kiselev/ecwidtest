package ru;

import org.apache.log4j.Logger;
import ru.constant.DownloadType;
import ru.entity.DownloadItem;
import ru.handler.FileHandler;
import ru.manage.Factory;
import ru.manage.interf.DownloadManager;

import java.io.File;
import java.util.List;

public class Application {
    private final static Logger logger = Logger.getLogger(Application.class);
    private static String fileName = null;
    private static String destinationDir = null;
    private static int threadCount = 0;
    private static String speed = null;
    private static DownloadType type = null;
    public static void main(String[]args ) throws Exception {
        int count = 0;
        long begin = System.currentTimeMillis();
        while (count < args.length){
            switch (args[count]){
                case "-n": count++; threadCount = Integer.valueOf(args[count]); break;
                case "-l": count++; speed = args[count]; break;
                case "-f": count++; fileName = args[count]; break;
                case "-o": count++; destinationDir = args[count]; break;
                case "-t": count++; type = DownloadType.valueOf(args[count]);
            }
            count++;
        }
        if (destinationDir == null){
            System.out.println("Need for destination dir");
            System.exit(0);
        }
        File dir = new File(destinationDir);
        if (!dir.exists()){
            if (!dir.mkdirs()){
                logger.error("can't create directory " + destinationDir);
                throw new Exception("can't create directory " + destinationDir);
            }
        }

        if (speed == null){
            System.out.println("Need for speed");
            System.exit(0);
        }
        int length = speed.length();
        int limitSpeed;
        if (Character.isLetter(speed.charAt(length - 1))) {
            switch (speed.substring(length - 1, length)) {
                case "k":
                    limitSpeed = Integer.valueOf(speed.substring(0, length - 1)) * 1024;
                    break;
                case "m":
                    limitSpeed = Integer.valueOf(speed.substring(0, length - 1)) * 1048576;
                    break;
                default:
                    limitSpeed = Integer.valueOf(speed.substring(0, length - 1));
            }
        } else {
            limitSpeed = Integer.valueOf(speed);
        }

        if (fileName == null){
            System.out.println("Need for file name");
            System.exit(0);
        }
        FileHandler handler = new FileHandler(fileName, destinationDir);
        List<DownloadItem> items = handler.prepareFile();
        if (threadCount <= 0){
            System.out.println("Need for thread count");
            System.exit(0);
        }
        if (items == null){
            System.out.println("cannot parse file " + fileName);
            System.exit(0);
        }
        if (type == null) type = DownloadType.FILE_PER_THREAD;
        List<DownloadManager> result = Factory.getManager(items, threadCount, limitSpeed, type);
        int downloaded = result.stream().map(DownloadManager::download).mapToInt(i -> i ).sum();
        long end = System.currentTimeMillis();
        System.out.println("Passed " + (end - begin)/1000.0f + " seconds");
        System.out.println("downloaded " + downloaded + " bytes");
    }
}