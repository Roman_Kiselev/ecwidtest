package ru.download;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DownloadResult {
    private Map<String, Boolean> resultHashMap = new ConcurrentHashMap<>();
    private volatile boolean result = true;

    public Map<String, Boolean> getResultHashMap() {
        return resultHashMap;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
