package ru.manage.impl;

import org.apache.log4j.Logger;
import ru.download.SingleFileDownloader;
import ru.entity.DownloadItem;
import ru.manage.interf.DownloadManager;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class DownloadManagerSingleFile implements DownloadManager{
    private final Logger logger = Logger.getLogger(this.getClass());
    private List<DownloadItem> downloadItems;
    public DownloadManagerSingleFile(List<DownloadItem> downloadItems){
        this.downloadItems = downloadItems;
    }


    @Override
    public Integer download() {
        ExecutorService exec = Executors.newFixedThreadPool(downloadItems.size());
        List<Future<Long>> result = downloadItems.stream().map(downloadItem ->
                exec.submit(new SingleFileDownloader(downloadItem.getFileName(), downloadItem.getUrl())))
                .collect(Collectors.toList());
        exec.shutdown();
        Long r = 0L;
        try{
            for (Future<Long> resultItem : result) {
                r += resultItem.get();
            }
            return r.intValue();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}
