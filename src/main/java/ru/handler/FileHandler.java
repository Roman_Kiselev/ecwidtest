package ru.handler;

import org.apache.log4j.Logger;
import ru.entity.DownloadItem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileHandler {
    private final Logger logger = Logger.getLogger(this.getClass());
    private String fileName;
    private String fileDestination;
    public FileHandler(String fileName, String fileDestination){
        this.fileName = fileName;
        this.fileDestination = fileDestination;
    }

    public List<DownloadItem> prepareFile() {

        try(Stream<String>file = Files.lines(Paths.get(fileName))){

            Function<String, DownloadItem> downloadItemFunction = (string -> {
                int index = string.indexOf(" ");
                String url = string.substring(0, string.indexOf(" "));
                String name = string.substring(index + 1, string.length());
                return new DownloadItem(url, fileDestination +  name);
            });

            return file.map(downloadItemFunction).collect(Collectors.toList());
        }catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}