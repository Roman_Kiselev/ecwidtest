package ru.manage;

import ru.constant.DownloadType;
import ru.download.DownloadGuard;
import ru.download.DownloadResult;
import ru.entity.DownloadItem;
import ru.manage.impl.DownloadManagerCopeFile;
import ru.manage.impl.DownloadManagerPartition;
import ru.manage.impl.DownloadManagerSingleFile;
import ru.manage.impl.DownloadManagerSingleFileMultithreading;
import ru.manage.interf.DownloadManager;

import java.util.*;
import java.util.stream.Collectors;

public class Factory {
    private static Map<String, String > items = new HashMap<>();
    private static DownloadGuard guard = null;
    public static List<DownloadManager> getManager(List<DownloadItem> downloadItems, int count, int speed,
                                                   DownloadType type){
        DownloadManager result = null;
        DownloadResult downloadResult = new DownloadResult();
        List<DownloadManager> resultList = new ArrayList<>();
        List<DownloadManager> finalResult = new ArrayList<>();
        List<DownloadItem> doubleItems = new ArrayList<>();
        boolean found;
        //search for duplicate files
        for (DownloadItem downloadItem: downloadItems) {
            Iterator<Map.Entry<String, String>> iterator = items.entrySet().iterator();
            found = false;
            Map.Entry<String, String> item;
            while (iterator.hasNext() && !found) {
                item = iterator.next();
                if (downloadItem.getUrl().equals(item.getValue())) {
                    found = true;
                    resultList.add(new DownloadManagerCopeFile(downloadItem.getFileName(), item.getKey(),
                            downloadResult));
                    doubleItems.add(downloadItem);
                }
            }
            items.put(downloadItem.getFileName(), downloadItem.getUrl());
        }
        //remove duplicate files
        List<DownloadItem> freeItems = downloadItems.stream().filter(
                downloadItem -> !doubleItems.contains(downloadItem)).collect(Collectors.toList());
        if (freeItems.size() > 0){
            switch (type){
                case PARTITION: result = new DownloadManagerPartition(freeItems, count, speed, downloadResult); break;
                case SINGLE: result = new DownloadManagerSingleFile(freeItems); break;
                case FILE_PER_THREAD: if (guard == null) guard = new DownloadGuard(speed);
                    result = new DownloadManagerSingleFileMultithreading(freeItems, count, guard,
                            downloadResult); break;
            }
            finalResult.add(result);
        }
        finalResult.addAll(resultList);
        return finalResult;
    }

}
