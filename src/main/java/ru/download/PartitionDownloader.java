package ru.download;

import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Callable;

public class PartitionDownloader implements Callable<Integer> {
    private final Logger logger = Logger.getLogger(this.getClass());
    private int endIndex;
    private String fileName;
    private String fileUrl;
    private int currentIndex;
    private DownloadGuard downloadGuard;
    private static final int BUFFER_SIZE = 1024;
    private DownloadResult downloadResult;
    public PartitionDownloader(int endIndex, String fileName, String url, int currentIndex, DownloadGuard downloadGuard,
                               DownloadResult downloadResult){
        this.endIndex = endIndex;
        this.fileName = fileName;
        this.fileUrl = url;
        this.currentIndex = currentIndex;
        this.downloadGuard = downloadGuard;
        this.downloadResult = downloadResult;
    }

    @Override
    public Integer call() throws IOException{
        File targetFile;
        synchronized (PartitionDownloader.class) {
            targetFile = new File(fileName);
            if (!targetFile.exists()) {
                if (!targetFile.createNewFile()){
                    System.out.println("Already created " + fileName);
                }
            }
        }
        byte[] buf = new byte[BUFFER_SIZE];
        URLConnection urlConnection;
        if (currentIndex < 0){
            if (endIndex == 0) return 0;
            else currentIndex = 0;
        }
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(targetFile, "rw")){
            URL url = new URL(fileUrl);
            int size = 0;
            urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Range", "bytes="
                    + currentIndex + "-" + endIndex);
            randomAccessFile.seek(currentIndex);
            try(BufferedInputStream bufferedInputStream = new BufferedInputStream(
                    urlConnection.getInputStream())) {
                boolean done = false;
                while (currentIndex < endIndex && !done && downloadResult.isResult()) {
                    long sleep = downloadGuard.canI();
                    if (sleep > 0) {
                        Thread.sleep(sleep);
                    }
                    int len = bufferedInputStream.read(buf, 0, BUFFER_SIZE);
                    if (len == -1)
                        done = true;
                    else {
                        randomAccessFile.write(buf, 0, len);
                        currentIndex += len;
                        size += len;
                    }
                    downloadGuard.setDownloaded(len);
                }
                return size;
            }
        } catch (IOException | InterruptedException e) {
            downloadResult.setResult(false);
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}
