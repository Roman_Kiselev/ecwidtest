package ru.download;

public class DownloadGuard {
    private long startTime;
    private int speed;
    private int size = 0;
    private static final long SECOND = 1000;

    public DownloadGuard(int speed) {
        this.speed = speed;
        startTime = System.currentTimeMillis();
    }

    synchronized long canI() {
        long currentTime = System.currentTimeMillis();
        long dif = currentTime - startTime;
        if (dif < SECOND) {
            if (size > speed) {
                return SECOND - dif;
            } else return 0;

        } else {
            startTime = currentTime;
            size = 0;
            return 0;
        }
    }

    synchronized void setDownloaded(int size) {
        this.size += size;
    }
}
