package ru.download;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.util.concurrent.Callable;

public class CopyFile implements Callable<Integer> {
    private final Logger logger = Logger.getLogger(this.getClass());

    private String fileDestination;
    private String fileSource;
    private static final long TWO_SECOND = 2000;
    private DownloadResult downloadResult;
    public CopyFile(String fileDestination, String fileSource, DownloadResult downloadResult){
        this.fileDestination = fileDestination;
        this.fileSource = fileSource;
        this.downloadResult = downloadResult;
    }
    @Override
    public Integer call() throws Exception {
        long currentSize;
        File source = new File(fileSource);
        Boolean isDone = downloadResult.getResultHashMap().get(fileSource);
        while (isDone == null){
            Thread.sleep(TWO_SECOND);
            isDone = downloadResult.getResultHashMap().get(fileSource);
        }
        if (!isDone) {
            logger.info(fileSource + " not downloaded");
            return 0;
        }
        try(FileInputStream file = new FileInputStream(source)) {
            currentSize = file.getChannel().size();
        }
        long oldSize = 0;
        while (oldSize != currentSize){
            try(FileInputStream file = new FileInputStream(new File(fileSource))) {
                oldSize = file.getChannel().size();
            }
            Thread.sleep(TWO_SECOND);
        }
        Files.copy(new File(fileSource).toPath(), new File(fileDestination).toPath());
        return 0;
    }
}
